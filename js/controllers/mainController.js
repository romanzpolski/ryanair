app.controller('MainController',[
    '$scope',
    'ApiService',
    'sourceAP',
    '$filter',
    function(
        $scope,
        ApiService,
        sourceAP,
        $filter
    )
    {
        $scope.sourceAP = '';
        $scope.destAP = '';
        $scope.callArgs = {};

        ApiService.getAirports().then(function(res){
            $scope.ap = res.airports;
            $scope.routes = res.routes;
            $scope.mess = res.messages;
            console.log($scope.mess);
        });


        $scope.findRoute = function(){
            var dests = $scope.routes[$scope.sourceAP];
            var destinations = [];
            angular.forEach(dests,function(item){
                var obj = $filter('filter')($scope.ap, {iatacode: item}, true)[0];
                destinations.push(obj);
            });
            //console.log(destinations);
            $scope.destinations = destinations;
        };

        $scope.$watch('sourceAP', function() {
            if($scope.sourceAP != ""){
                $scope.findRoute();
            }
        });

        formatDate = function(date) {
            var day = ("0" + (date.getDate() + 1)).slice(-2);
            var month = ("0" + (date.getMonth() + 1)).slice(-2);
            var year = date.getFullYear();
            return year+"-"+month+"-"+day;
        }

        $scope.prepareCall = function (){
            var args = {};
            args.sap = $scope.sourceAP;
            args.dap = $scope.destAP;
            args.sd = formatDate($scope.departureDate);
            args.ed = formatDate($scope.comebackDate);
            $scope.callArgs = args;
        }

        $scope.$watch('callArgs', function() {
            if($scope.callArgs.ed != "") {
                ApiService.getFlights($scope.callArgs).then(function(res){
                    $scope.flights = res;
                    console.log(res);
                });
            }
        });

    }]);
