'use strict';
var models = angular.module('models',[]);
var services = angular.module('services',[]);
var app = angular.module('flights',[
    'models',
    'services',
    'ngRoute',
    'ngAnimate',
    'ngMaterial',
    //'angular-moment'
]);

app.config(function ($routeProvider, $locationProvider, $mdDateLocaleProvider){
    $routeProvider
        .when('/', {
            templateUrl: 'js/templates/main.html',
            controller: 'MainController'
        })
        .otherwise({
            redirectTo: '/'
        });

    // $mdDateLocaleProvider.formatDate = function(date) {
    //   //  return amMoment(date).format('YYYY-MM-DD');
    //     var mydate = new Date(date);
    //
    // };
});

