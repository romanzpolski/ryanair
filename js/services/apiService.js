'use strict';
services.factory('ApiService',['$http','$q','sourceAP',
    function($http,$q,sourceAP){

        var ApiService = {};

        ApiService.getAirports = function(){
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: 'https://murmuring-ocean-10826.herokuapp.com/en/api/2/forms/flight-booking-selector/'
            }).then(function(result){
                var res = {};
                var airports = [];
                //console.log(result);
                angular.forEach(result.data.airports,function(item){
                    var obj = new sourceAP(item);
                    airports.push(obj);
                });
                res.airports = airports;
                res.routes = result.data.routes;
                res.messages = result.data.messages;
                deferred.resolve(res);
            },function(error){
                console.log(error+ "1sdasd");
                deferred.reject();
            });
            return deferred.promise;
        }

        ApiService.getFlights = function(args){
            //var url = "";
            var deferred = $q.defer();
            $http.get('https://murmuring-ocean-10826.herokuapp.com/en/api/2/flights/from/'+args.sa+'/to/'+args.da+'/'+args.sd+'/'+args.ed+'/250/unique/?limit=15&offset-0').then(function(result){
                deferred.resolve(result.data.flights);
            },function(error){
                console.log(error+ "2sdasd");
                deferred.reject();
            });
            return deferred.promise;
        }

        return ApiService;
    }]);
