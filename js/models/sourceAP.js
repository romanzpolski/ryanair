'use strict';
models.factory('sourceAP',['$q','$injector',function($q,$injector){
    var sourceAP = function(jsonObj){

        this.iatacode = jsonObj.iataCode;
        this.name = jsonObj.name;
        this.country = jsonObj.country;
    };

    return sourceAP;
}]);
